const { series, src, dest, watch } = require("gulp");

const sass = require("gulp-sass");
const imagemin = require("gulp-imagemin");
const webp = require("gulp-webp");
const concat = require("gulp-concat");

//Utilidades css
const autoprefixer = require("autoprefixer");
const postcss = require("gulp-postcss");
const cssnano = require("cssnano");

function css() {
  return src("./src/scss/app.scss")
    .pipe(sass({ outputStyle: "expanded" }))
    .pipe(postcss([autoprefixer(), cssnano()]))
    .pipe(dest("./build/css"));
}
function expandCSS() {
  return src("./src/scss/app.scss")
    .pipe(sass({ outputStyle: "expanded" }))
    .pipe(dest("./build/css"));
}
function javascript() {
  return src("./src/js/**/*")
    .pipe(concat("bundle.js")) //anida js
    .pipe(dest("./build/js"));
}
function minificarCSS() {
  return src("./src/scss/app.scss")
    .pipe(sass({ outputStyle: "compressed" })) //Compressed para rendimiento
    .pipe(dest("./build/css"));
}

function imagenes() {
  return src("./src/img/**/*")
    .pipe(imagemin()) //hace mas rapido minificando img
    .pipe(dest("./build/img"));
}

function versionWebp() {
  return src("./src/img/**/*")
    .pipe(webp()) //Webp
    .pipe(dest("./build/img"));
}

function watchArchivo() {
  watch("./src/scss/**/*.scss", css);
  watch("./src/js/**/*", javascript);
}

exports.css = css;
exports.minificarCSS = minificarCSS;
exports.imagenes = imagenes;
exports.versionWebp = versionWebp;
exports.watchArchivo = watchArchivo;
exports.javascript = javascript;
exports.expandCSS = expandCSS;
