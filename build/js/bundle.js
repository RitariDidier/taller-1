document.addEventListener("DOMContentLoaded", function () {
  scrollNav();
});

function scrollNav() {
  const enlaces = document.querySelectorAll(".navegacion a");

  enlaces.forEach(function (enlace) {
    enlace.addEventListener("click", function (e) {
      e.preventDefault();

      const seccion = document.querySelector(e.target.attributes.href.value);

      seccion.scrollIntoView({
        behavior: "smooth",
      });
    });
  });
}


const nombre = document.querySelector("#nombre");
const mensaje = document.querySelector("#mensaje");

const formulario = document.querySelector(".formulario");

const datos = {
  nombre: "",
  mensaje: "",
};

nombre.addEventListener("input", leerTexto);
mensaje.addEventListener("input", leerTexto);

function leerTexto(e) {
  datos[e.target.id] = e.target.value;
}

formulario.addEventListener("submit", function (evento) {
  evento.preventDefault();

  //Validar Formulario
  const { nombre, mensaje } = datos;

  if (nombre === "" || mensaje === "") {
    mostrarAlerta("Todos los campos son obligatorios", "error");

    return;
  } else {
    const nlength = nombre.length;
    const mlength = mensaje.length;

    if (nlength <= 3 || mlength >= 20) {
      if (nlength <= 3) {
        mostrarAlerta("El nombre debe ser mayor a 3 ", "error");
        return;
      }
      if (mlength >= 20) {
        mostrarAlerta("El mensaje Debe ser menor a 20 letras", "error");
        return;
      }
    }
  }
  //*Format Date//
  var fecha = calcularFecha();

  //*enviar Formulario
  mostrarAlerta("Comentario Agregado", "correcto");

  //*crearComentario en seccion
  crearComentarios(nombre, mensaje, fecha);
});

function mostrarAlerta(mensaje, estilo) {
  const alerta = document.createElement("p");
  alerta.classList.add(estilo);
  alerta.textContent = mensaje;

  formulario.appendChild(alerta);

  setTimeout(() => {
    alerta.remove();
  }, 3000);
}
const comentarios = document.querySelector(".comentarios");

function crearComentarios(nombre, mensaje, fecha) {
  const comentarioNombre = document.createElement("label");
  const comentarioMensaje = document.createElement("P");
  const comentarioFecha = document.createElement("label");

  comentarioFecha.textContent = "Fecha: " + fecha;
  comentarioNombre.textContent = nombre;
  comentarioMensaje.textContent = mensaje;

  const comentarios__header = document.createElement("DIV");

  comentarios__header.classList.add("comentarios__header");
  comentarios__header.appendChild(comentarioNombre);
  comentarios__header.appendChild(comentarioFecha);

  const cmt = document.createElement("LI");
  cmt.classList.add("campo__comentario");
  var x = document.querySelector(".comentarios").childElementCount;

  if (x <= 3) {
    cmt.appendChild(comentarios__header);
    cmt.appendChild(comentarioMensaje);
    comentarios.appendChild(cmt);
    return;
  } else {
    var x = document.querySelector(".comentarios");
    x.removeChild(x.childNodes[0]);
    cmt.appendChild(comentarios__header);
    cmt.appendChild(comentarioMensaje);
    comentarios.prepend(cmt);
  }
}

function calcularFecha() {
  let date = new Date();
  var options = {
    year: "numeric",
    month: "long",
    day: "numeric",
    hour: "numeric",
    minute: "numeric",
  };
  var fecha = date.toLocaleDateString("es-ES", options);
  return fecha;
}

document.addEventListener("DOMContentLoaded", function () {
  crearGaleria();
});

function crearGaleria() {
  const galeria = document.querySelector(".galeria");

  for (let index = 1; index <= 7; index++) {
    const imagen = document.createElement("IMG");

    imagen.src = `build/img/${index}.webp`;
    imagen.dataset.imagenId = index;
    //agregar Descripcion
    agregarDescripcion(imagen);
    imagen.onclick = mostrarImagen;

    const lista = document.createElement("LI");

    if (index === 6) {
      lista.classList.add("cascada"); //Agrego clase para poder ubicarla
    }

    lista.appendChild(imagen);

    galeria.appendChild(lista);
  }
}

function mostrarImagen(e) {
  const id = parseInt(e.target.dataset.imagenId);
  const desc = e.target.dataset.imagenDescripcion;

  const descripcion = document.createElement("P");
  descripcion.textContent = desc;
  descripcion.classList.add("imagen_descripcion");

  const imagen = document.createElement("IMG");
  imagen.src = `build/img/${id}.webp`;

  const overlay = document.createElement("DIV");
  overlay.appendChild(imagen);
  overlay.appendChild(descripcion);
  overlay.classList.add("overlay");

  //boton cerrar
  const cerrarImagen = document.createElement("p");
  cerrarImagen.textContent = "X";
  cerrarImagen.classList.add("btn-cerrar");

  //cerrar imagen
  cerrarImagen.onclick = function () {
    overlay.remove();
  };

  //boton Cerrar
  overlay.appendChild(cerrarImagen);

  //Mostrar en el html
  const body = document.querySelector("body");
  body.appendChild(overlay);
}

function agregarDescripcion(imagen) {
  const id = parseInt(imagen.dataset.imagenId);
  switch (id) {
    case 1:
      imagen.dataset.imagenDescripcion = "Parque Nacional - Huerquehue";
      break;
    case 2:
      imagen.dataset.imagenDescripcion = "Parque Nacional - Conguillio";
      break;
    case 3:
      imagen.dataset.imagenDescripcion = "Cerro Diuco - Coñaripe";
      break;
    case 4:
      imagen.dataset.imagenDescripcion = "Noche estrellada - Villarrica";
      break;
    case 5:
      imagen.dataset.imagenDescripcion = "Cerro Diuco - Coñaripe";
      break;
    case 6:
      imagen.dataset.imagenDescripcion =
        "Salto del Aguila - Parque Nacional - Huerquehue";
      break;
    case 7:
      imagen.dataset.imagenDescripcion = "Parque Nacional - Huerquehue";
      break;
    default:
      break;
  }
}
