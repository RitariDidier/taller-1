const nombre = document.querySelector("#nombre");
const mensaje = document.querySelector("#mensaje");

const formulario = document.querySelector(".formulario");

const datos = {
  nombre: "",
  mensaje: "",
};

nombre.addEventListener("input", leerTexto);
mensaje.addEventListener("input", leerTexto);

function leerTexto(e) {
  datos[e.target.id] = e.target.value;
}

formulario.addEventListener("submit", function (evento) {
  evento.preventDefault();

  //Validar Formulario
  const { nombre, mensaje } = datos;

  if (nombre === "" || mensaje === "") {
    mostrarAlerta("Todos los campos son obligatorios", "error");

    return;
  } else {
    const nlength = nombre.length;
    const mlength = mensaje.length;

    if (nlength <= 3 || mlength >= 20) {
      if (nlength <= 3) {
        mostrarAlerta("El nombre debe ser mayor a 3 ", "error");
        return;
      }
      if (mlength >= 20) {
        mostrarAlerta("El mensaje Debe ser menor a 20 letras", "error");
        return;
      }
    }
  }
  //*Format Date//
  var fecha = calcularFecha();

  //*enviar Formulario
  mostrarAlerta("Comentario Agregado", "correcto");

  //*crearComentario en seccion
  crearComentarios(nombre, mensaje, fecha);
});

function mostrarAlerta(mensaje, estilo) {
  const alerta = document.createElement("p");
  alerta.classList.add(estilo);
  alerta.textContent = mensaje;

  formulario.appendChild(alerta);

  setTimeout(() => {
    alerta.remove();
  }, 3000);
}
const comentarios = document.querySelector(".comentarios");

function crearComentarios(nombre, mensaje, fecha) {
  const comentarioNombre = document.createElement("label");
  const comentarioMensaje = document.createElement("P");
  const comentarioFecha = document.createElement("label");

  comentarioFecha.textContent = "Fecha: " + fecha;
  comentarioNombre.textContent = nombre;
  comentarioMensaje.textContent = mensaje;

  const comentarios__header = document.createElement("DIV");

  comentarios__header.classList.add("comentarios__header");
  comentarios__header.appendChild(comentarioNombre);
  comentarios__header.appendChild(comentarioFecha);

  const cmt = document.createElement("LI");
  cmt.classList.add("campo__comentario");
  var x = document.querySelector(".comentarios").childElementCount;

  if (x <= 3) {
    cmt.appendChild(comentarios__header);
    cmt.appendChild(comentarioMensaje);
    comentarios.appendChild(cmt);
    return;
  } else {
    var x = document.querySelector(".comentarios");
    x.removeChild(x.childNodes[0]);
    cmt.appendChild(comentarios__header);
    cmt.appendChild(comentarioMensaje);
    comentarios.prepend(cmt);
  }
}

function calcularFecha() {
  let date = new Date();
  var options = {
    year: "numeric",
    month: "long",
    day: "numeric",
    hour: "numeric",
    minute: "numeric",
  };
  var fecha = date.toLocaleDateString("es-ES", options);
  return fecha;
}
