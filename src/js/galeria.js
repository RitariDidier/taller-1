document.addEventListener("DOMContentLoaded", function () {
  crearGaleria();
});

function crearGaleria() {
  const galeria = document.querySelector(".galeria");

  for (let index = 1; index <= 7; index++) {
    const imagen = document.createElement("IMG");

    imagen.src = `build/img/${index}.webp`;
    imagen.dataset.imagenId = index;
    //agregar Descripcion
    agregarDescripcion(imagen);
    imagen.onclick = mostrarImagen;

    const lista = document.createElement("LI");

    if (index === 6) {
      lista.classList.add("cascada"); //Agrego clase para poder ubicarla
    }

    lista.appendChild(imagen);

    galeria.appendChild(lista);
  }
}

function mostrarImagen(e) {
  const id = parseInt(e.target.dataset.imagenId);
  const desc = e.target.dataset.imagenDescripcion;

  const descripcion = document.createElement("P");
  descripcion.textContent = desc;
  descripcion.classList.add("imagen_descripcion");

  const imagen = document.createElement("IMG");
  imagen.src = `build/img/${id}.webp`;

  const overlay = document.createElement("DIV");
  overlay.appendChild(imagen);
  overlay.appendChild(descripcion);
  overlay.classList.add("overlay");

  //boton cerrar
  const cerrarImagen = document.createElement("p");
  cerrarImagen.textContent = "X";
  cerrarImagen.classList.add("btn-cerrar");

  //cerrar imagen
  cerrarImagen.onclick = function () {
    overlay.remove();
  };

  //boton Cerrar
  overlay.appendChild(cerrarImagen);

  //Mostrar en el html
  const body = document.querySelector("body");
  body.appendChild(overlay);
}

function agregarDescripcion(imagen) {
  const id = parseInt(imagen.dataset.imagenId);
  switch (id) {
    case 1:
      imagen.dataset.imagenDescripcion = "Parque Nacional - Huerquehue";
      break;
    case 2:
      imagen.dataset.imagenDescripcion = "Parque Nacional - Conguillio";
      break;
    case 3:
      imagen.dataset.imagenDescripcion = "Cerro Diuco - Coñaripe";
      break;
    case 4:
      imagen.dataset.imagenDescripcion = "Noche estrellada - Villarrica";
      break;
    case 5:
      imagen.dataset.imagenDescripcion = "Cerro Diuco - Coñaripe";
      break;
    case 6:
      imagen.dataset.imagenDescripcion =
        "Salto del Aguila - Parque Nacional - Huerquehue";
      break;
    case 7:
      imagen.dataset.imagenDescripcion = "Parque Nacional - Huerquehue";
      break;
    default:
      break;
  }
}
